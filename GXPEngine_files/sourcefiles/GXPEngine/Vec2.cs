﻿using System;
using GXPEngine;

public class Vec2
{
    public static Vec2 zero { get { return new Vec2(0, 0); } }

    public float x = 0;
    public float y = 0;

    private float _length;

    private static Random rng = new Random();

    public Vec2(float pX = 0, float pY = 0)
    {
        x = pX;
        y = pY;
    }

    public override string ToString()
    {
        return String.Format("({0}, {1})", x, y);
    }

    public Vec2 Add(Vec2 other)
    {
        x += other.x;
        y += other.y;
        return this;
    }

    public Vec2 Subtract(Vec2 other)
    {
        x -= other.x;
        y -= other.y;
        return this;
    }

    public Vec2 Scale(float pScale)
    {
        x *= pScale;
        y *= pScale;
        return this;
    }

    public float GetLength()
    {
        _length = Mathf.Sqrt((x * x) + (y * y));
        return _length;
    }

    public Vec2 Normalize()
    {
        float originalLength = GetLength();
        x = x / originalLength;
        y = y / originalLength;
        return new Vec2(x, y); ;
    }

    public Vec2 Clone()
    {
        return new Vec2(x, y);
    }

    public Vec2 SetXY(Vec2 other)
    {
        x = other.x;
        y = other.y;
        return this;
    }

    public Vec2 SetXY(float pX, float pY)
    {
        x = pX;
        y = pY;
        return this;
    }

    public static float Deg2Rad(float pDegrees)
    {
        return pDegrees * (Mathf.PI / 180);
    }

    public static float Rad2Deg(float pRadians)
    {
        return pRadians * (180 / Mathf.PI);
    }

    public static Vec2 GetUnitVectorDegrees(float pDegrees)
    {
        return new Vec2(Mathf.Cos(pDegrees * (Mathf.PI / 180)), Mathf.Sin(pDegrees * (Mathf.PI / 180)));
    }

    public static Vec2 GetUnitVectorRadians(float pRadians)
    {
        return new Vec2(Mathf.Cos(pRadians), Mathf.Sin(pRadians));
    }

    public static Vec2 RandomUnitVector()
    {
        float randomAngle = rng.Next(0, 360);
        return GetUnitVectorDegrees(randomAngle);
    }

    public Vec2 SetAngleDegrees(float pDegrees)
    {
        Vec2 newVector = GetUnitVectorDegrees(pDegrees).Scale(GetLength());
        return newVector;
    }

    public Vec2 SetAngleRadians(float pRadians)
    {
        Vec2 newVector = GetUnitVectorRadians(pRadians).Scale(GetLength());
        return newVector;
    }

    public float GetAngleDegrees() // TODO: same error as in written test... How to solve it?
    {
        if (x > 0)
        {
            return Mathf.Atan(y / x) * (180 / Mathf.PI);
        }
        else
        {
            return Mathf.Atan(y / x) * (180 / Mathf.PI) + 180;
        }
    }

    public float GetAngleRadians()
    {
        if (x > 0)
        {
            return Mathf.Atan(y / x);
        }
        else
        {
            return Mathf.Atan(y / x) + Mathf.PI;
        }
    }

    public Vec2 RotateDegrees(float pDegrees)
    {
        float newAngle = GetAngleDegrees() + pDegrees;
        return SetAngleDegrees(newAngle);
    }

    public Vec2 RotateRadians(float pRadains)
    {
        float newAngle = GetAngleRadians() + pRadains;
        return SetAngleRadians(newAngle);
    }

    public Vec2 RotateAroundDegrees(Vec2 P, float pDegrees)
    {
        Vec2 newVector = new Vec2(x + P.x, y + P.y).RotateDegrees(pDegrees).Subtract(P);
        return newVector;
    }

    public Vec2 RotateAroundRadians(Vec2 P, float pRadians)
    {
        Vec2 newVector = new Vec2(x + P.x, y + P.y).RotateRadians(pRadians).Subtract(P);
        return newVector;
    }
}

