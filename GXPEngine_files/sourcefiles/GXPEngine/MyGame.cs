using System;
using System.Drawing;
using GXPEngine;

public class MyGame : Game 
{
    public Player player;

	public MyGame() : base(1100, 775, false)
	{
        player = new Player();
        AddChild(player);
	}

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            shoot();
        }
    }

    private void shoot()
    {
        Bullet bullet = new Bullet();
        AddChild(bullet);
    }

    static void Main()
    {
        new MyGame().Start();
    }
}
