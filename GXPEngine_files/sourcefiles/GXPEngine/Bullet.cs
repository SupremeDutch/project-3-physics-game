﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GXPEngine;

public class Bullet : Sprite
{
    private Vec2 _position;
    private Vec2 _velocity;

    private Vec2 _bulletSpeed = new Vec2(15,0);

    public Bullet() : base("square.png")
    {
        _myGame = game as MyGame;
        scale = 0.15f;

        rotation = _myGame.player.rotation;
        _position = _myGame.player._position.Clone();
        _velocity = _bulletSpeed.RotateDegrees(rotation);
    }

    private void Update()
    {
        if (x < 0 || x > _myGame.width)
        {
            Destroy();
        }
        if (y < 0 || y > _myGame.height)
        {
            Destroy();
        }

        Step();
    }

    public void Step()
    {
        _position.Add(_velocity);

        x = _position.x;
        y = _position.y;
    }
}
