﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GXPEngine;

public class Player : Sprite
{
    public Vec2 _position { get; private set; }
    public Vec2 _velocity { get; private set; }

    public Player() : base("player.png")
    {
        _myGame = game as MyGame;

        SetOrigin(width / 2, height / 2);

        _velocity = new Vec2(0, 0);
        _position = new Vec2(_myGame.width / 2, _myGame.height / 2);
    }

    private void Update()
    {
        
        movement();
        rotatePlayer();
        Step();
    }

    private void movement()
    {
        if (Input.GetKey(Key.A))
        {
            _velocity.x -= 1;
        }
        if (Input.GetKey(Key.D))
        {
            _velocity.x += 1;
        }
        if (Input.GetKey(Key.W))
        {
            _velocity.y -= 1;
        }
        if (Input.GetKey(Key.S))
        {
            _velocity.y += 1;
        }

        _velocity.x *= 0.9f;
        _velocity.y *= 0.9f;
    }

    private void rotatePlayer()
    {
        Vec2 deltaVec = new Vec2(Input.mouseX, Input.mouseY).Subtract(_position);
        rotation = deltaVec.GetAngleDegrees();
    }

    

    public void Step()
    {
        _position.Add(_velocity);

        x = _position.x;
        y = _position.y;
    }
}
